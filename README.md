# Entity Reference Views Table

This module leverage on the Entity Reference views display in order to edit a node entity reference field using a view.

It adds a field widget named _Table_ and a views/row Plugin to show the list of referenced entities in a table.

The table has the settled Entity Reference views display fields and an additional checkbox (for each row) to select the wanted entities.

## Usage

Set up a node content and a view with an Entity Reference views display like explained here
https://www.ostraining.com/blog/drupal/drupal-8-entity-reference-view/

The result is Autocoplete or a Select with additional fields taken from the view

With this module enabled:

- Create a View
- Add a Entity reference display and set the FORMAT -> Format _Entity Reference list_ (pay attention to select a field inside the settings  like a common Entity reference display configuration)
  In the FORMAT -> Show select _Entity Reference table fields_ (the settings for this element does not work now)
- Go to the node with the referenced field
  - In the Manage field select the Views: Entity reference display
  - In the Manage Form display select the new _Table_ widget
