<?php

namespace Drupal\entity_reference_views_table\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase;
use \Drupal\node\Entity\Node;
use Drupal\views\Views;

/**
 * Plugin implementation of the 'TableViewsWidget' widget.
 *
 * @FieldWidget(
 *   id = "entity_reference_views_table",
 *   label = @Translation("Table"),
 *   field_types = {
 *     "entity_reference"
 *   },
 *   multiple_values = TRUE
 * )
 */
class EntityReferenceTableSelectViewsWidget extends OptionsWidgetBase {
  // entity_reference_views_table
  // We don't use an extension of EntityReferenceAutocompleteWidget
  // because it has not the getOptions() method that is used here
  // to get all the referenced entities to create the table

  /**
   * {@inheritdoc}
   */
  // public static function defaultSettings() {
  //   return [
  //       // Implement default settings.
  //       // Settings can be related to the position of the slect checkbox in the table
  //       'position' => '...', // TODO
  //     ] + parent::defaultSettings();
  // }
  //
  //
  // /**
  //  * {@inheritdoc}
  //  */
  // public function settingsSummary() {
  //   $position = $this->getSetting('position');
  //   $summary = [];
  //   $summary[] = t('Table select column @position', [
  //     '@position' => $this->getSetting('position'),
  //   ]);
  //   return $summary;
  // }

  /**
   * {@inheritdoc}
   */
  // public function settingsForm(array $form, FormStateInterface $form_state) {
  //   $form = parent::settingsForm($form, $form_state);
  //   $form['position'] = [
  //     '#title' => t('Position of checkbox column'),
  //     '#type' => 'select',
  //     '#options' => [] // TODO
  //     '#required' => TRUE,
  //     '#description' => 'Where to display the checbox column in the table',
  //     '#default_value' => $this->getSetting('position'),
  //   ];
  //   return $form;
  // }

  /**
   * Gives a visual Table to save Analysis.
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    /**
    / Get settings from field edit to get the Entity reference view display info
    **/

    // Get field settings - These are the field->edit->entity reference field->edit settings
    // where the EntityReference view display is selected
    if($this->fieldDefinition->getSettings()['handler'] == "views") {
      $view_id = $this->fieldDefinition->getSettings()['handler_settings']['view']['view_name'];
      $display_id = $this->fieldDefinition->getSettings()['handler_settings']['view']['display_name'];
      $args = $this->fieldDefinition->getSettings()['handler_settings']['view']['arguments'];
    }

    /**
    / Get the referenced items elements
    **/
    // Get a list of all the referenced entities
    $options = $this->getOptions($items->getEntity());
    // Get a list of selected referenced entities oprions
    $selected = $this->getSelectedOptions($items);

    /**
    // View load and Table render
    **/

    $view = Views::getView($view_id);

      if (is_object($view)) {

        // Get fields used in view - Maybe is possible to use other approach
        $display = $view->storage->get('display')[$display_id];
        // generate table header from this array info
        foreach ($display['display_options']['fields'] as $id => $value) {
          if(!$value['exclude']){
            $header[] = $value['label'];
          }
        }

        // Append a column for checkbox
        // TODO - the column can be prepended - see settings
        $header[] = ['-'];

        $view->setArguments($args);
        $view->setDisplay($display_id);
        $view->preExecute();
        $view->execute();
        $view->preview();

        // Get the custom EntityReference row plugin renderer
        // This is used  just to override the theme
        // (views/row/EntityReference.php row plugin look at @annotation)
        // Maybe is possible to use template suggestion
        $renderer = $view->rowPlugin;
        if($renderer) {
          $table = [
             '#type' => 'table',
             '#header' => $header,
          ];

          foreach ($view->result as $index => $row) {
            $view->row_index = $index;
            $renderarray = $renderer->render($row);

            $table[$index]['content'] = array(
              // TODO drupal_render is deprecated
              '#markup' => drupal_render($renderer->render($row)),
            );
            if(in_array($row->nid,$selected)) {
              $table[$index]['target_id'] = array(
                '#type' => 'checkbox',
                '#attributes' => array("checked" => "checked"),
                '#return_value' => $row->nid
              );
            } else {
              $table[$index]['target_id'] = array(
                '#type' => 'checkbox',
                '#return_value' => $row->nid
              );
            }

          }
        }
    }

    return $table;
  }
}
